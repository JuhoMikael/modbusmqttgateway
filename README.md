 * Version 0.1
 
 
 # ModbusTCP-MQTT Gateway 
 
 Modbus-MQTT Gateway project researches the possibility of interconnecting existing automation networks with IoT networks with simple protocol converter. 
 
 The protocol converter relies on Jamod modbus and Xenqtt MQTT libraries. It has been successfully tested on Raspberry Pi2. 
 
 Further development is needed for acquiring a responsive RTU configuration scheme.
 

 ###  Dependencies 
 
 
 http://xenqtt.sourceforge.net
 
    xenqtt-0.9.6.jar
 
 https://sourceforge.net/projects/jamod/files/jamod/1.2/ 
 
    jamod-1.2-SNAPSHOT.jar
 

 
 ### Owner 
 
 * Juho
 
 