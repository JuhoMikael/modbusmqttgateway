package com.JuhoMikael.ModbusMQTTGateway;


import java.net.ConnectException;
import java.net.InetAddress;

import net.wimpi.modbus.ModbusCoupler;
import net.wimpi.modbus.io.ModbusTCPTransaction;
import net.wimpi.modbus.msg.ReadInputDiscretesRequest;
import net.wimpi.modbus.msg.ReadInputDiscretesResponse;
import net.wimpi.modbus.msg.ReadInputRegistersRequest;
import net.wimpi.modbus.msg.ReadInputRegistersResponse;
import net.wimpi.modbus.msg.WriteCoilRequest;
import net.wimpi.modbus.msg.WriteCoilResponse;
import net.wimpi.modbus.net.ModbusSerialListener;
import net.wimpi.modbus.net.TCPMasterConnection;
import net.wimpi.modbus.procimg.SimpleProcessImage;
import net.wimpi.modbus.util.SerialParameters;



public class ModbusRTUSlave extends Thread{


/* Class for establishing Modbus Connectivity with Modbus RTU & Serial line */
	
	public void run(){
	
		
		
	try {
	     
		ModbusSerialListener listener = null;
		slaveProcessImage kuva = new slaveProcessImage();
		SimpleProcessImage spi = kuva.spi;
		
		ModbusCoupler.getReference().setProcessImage(spi);
		ModbusCoupler.getReference().setMaster(false);
		ModbusCoupler.getReference().setUnitID(2);
	    
		SerialParameters separam = new SerialParameters();
		separam.setPortName("/dev/ttyUSB0");
		separam.setBaudRate(9600);
		separam.setDatabits(8);
		separam.setParity("None");
		separam.setStopbits(1);
		separam.setEncoding("ascii");
		separam.setEcho(false);
		
		listener = new ModbusSerialListener(separam);
		listener.setListening(true);
	}
	    
	   
		
	    catch (Exception ex) {
	      ex.printStackTrace();
	    }
}
}

