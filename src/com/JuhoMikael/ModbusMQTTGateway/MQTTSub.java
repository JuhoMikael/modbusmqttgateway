package com.JuhoMikael.ModbusMQTTGateway;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.sf.xenqtt.client.MqttClient;
import net.sf.xenqtt.client.MqttClientListener;
import net.sf.xenqtt.client.PublishMessage;
import net.sf.xenqtt.client.Subscription;
import net.sf.xenqtt.client.SyncMqttClient;
import net.sf.xenqtt.message.ConnectReturnCode;
import net.sf.xenqtt.message.QoS;

public class MQTTSub {
String address;
String address2;
String address3;
String address4;
String address5;
String value = "0";
String value2 = "0";
String value3 = "0";
String reVal1 = "0";
String reVal2 = "0";


public String getValue(){
	return value;
}

public String getValue2(){
	return value2;
}

public String getValue3(){
	return value3;
}

public boolean getDI(){
	if(value.equals("1") || value == "1" || value.charAt(0) == '1'){
		return true;
	}
	else{
		return false;
	}
}
	
public boolean getDI2(){
	if(value2.equals("1") || value2 == "1" || value2.charAt(0) == '1'){
		return true;
	}
	else{
		return false;
	}
}
	
public boolean getDI3(){
	if(value3.equals("1") || value3 == "1" || value3.charAt(0) == '1'){
		return true;
	}
	else{
		return false;
	}
}

public String getReg1(){
	return reVal1;
}
public String getReg2(){
	return reVal2;
}

public void mqttRef(String topic, String topic2, String topic3, String topic4, String topic5){
	
	address =topic;
	address2 =topic2;
	address3 = topic3;
	address4 = topic4;
	address5 = topic5;
	
	final List<String> catalog = Collections.synchronizedList(new ArrayList<String>());
	MqttClientListener listener = new MqttClientListener() {

		@Override
		public void publishReceived(MqttClient client, PublishMessage message) {
			catalog.add(message.getPayloadString());
			if(message.getTopic().equals("di1")){
			value = message.getPayloadString();}
			else if(message.getTopic().equals("di2")){
			value2 = message.getPayloadString();
			}
			else if(message.getTopic().equals("di3")){
			value3 = message.getPayloadString();
			}
			else if(message.getTopic().equals("re1")){
				reVal1 = message.getPayloadString();
			}
			else if(message.getTopic().equals("re2")){
				reVal2 = message.getPayloadString();
			}
			else{
				System.out.println("Error1: MQTT Read Error");
			}
			
			System.out.println("FromMQTT: "+message.getPayloadString());
			message.ack();
		}

		@Override
		public void disconnected(MqttClient client, Throwable cause, boolean reconnecting) {
			if (cause != null) {
				System.out.println("Disconnected from the broker due to an exception.");
			} else {
				System.out.println("Disconnecting from the broker.");
			}

			if (reconnecting) {
				System.out.println("Attempting to reconnect to the broker.");
			}
		}

	};

	// Build your client. This client is a synchronous one so all interaction with the broker will block until said interaction completes.
	//SyncMqttClient client = new SyncMqttClient("tcp://82.181.145.144:1883", listener, 5);
	SyncMqttClient client = new SyncMqttClient("tcp://192.168.11.64:1883", listener, 5);
	try {
		// Connect to the broker with a specific client ID. Only if the broker accepted the connection shall we proceed.
		ConnectReturnCode returnCode = client.connect("modbusReference", true);
		if (returnCode != ConnectReturnCode.ACCEPTED) {
			System.out.println("Unable to connect to the MQTT broker. Reason: " );
			return;
		}

		// Create your subscriptions. In this case we want to build up a catalog of classic rock.
		List<Subscription> subscriptions = new ArrayList<Subscription>();
		subscriptions.add(new Subscription(address, QoS.AT_MOST_ONCE));
		subscriptions.add(new Subscription(address2, QoS.AT_MOST_ONCE));
		subscriptions.add(new Subscription(address3, QoS.AT_MOST_ONCE));
		subscriptions.add(new Subscription(address4, QoS.AT_MOST_ONCE));
		subscriptions.add(new Subscription(address5, QoS.AT_MOST_ONCE));
		System.out.println("Subscribed to: "+ address+"&&" + address2 + "&&" + address3+"&&" + address4 + "&&" + address5);
		
		client.subscribe(subscriptions);
		

		// Build up your catalog. After a while you've waited long enough so move on.
		while(true){
			Thread.sleep(1500);
			
		}
	
	} catch (Exception ex) {
		System.out.println("An unexpected exception has occurred.");
	}
	
	
}
}
