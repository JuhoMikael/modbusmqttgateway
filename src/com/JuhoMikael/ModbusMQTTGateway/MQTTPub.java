package com.JuhoMikael.ModbusMQTTGateway;


import net.sf.xenqtt.client.MqttClient;
import net.sf.xenqtt.client.MqttClientListener;
import net.sf.xenqtt.client.PublishMessage;
import net.sf.xenqtt.client.SyncMqttClient;
import net.sf.xenqtt.message.ConnectReturnCode;
import net.sf.xenqtt.message.QoS;

public class MQTTPub {


	public void publish(String topics, String messages){
	
	MqttClientListener listener = new MqttClientListener() {

		@Override
		public void publishReceived(MqttClient client, PublishMessage message) {
			System.out.println("");
		}

		@Override
		public void disconnected(MqttClient client, Throwable cause, boolean reconnecting) {
			if (cause != null) {
				System.out.println("Disconnected from the broker due to an exception.");
			} else {
				System.out.println("Disconnected from the broker.");
			}

			if (reconnecting) {
				System.out.println("Attempting to reconnect to the broker.");
			}
		}
	};

	// Build your client. This client is a synchronous one so all interaction with the broker will block until said interaction completes.
	MqttClient client = new SyncMqttClient("tcp://192.168.11.64:1883", listener, 5);
	//MqttClient client = new SyncMqttClient("tcp://82.181.145.144:1883", listener, 5);
	try {
		ConnectReturnCode returnCode = client.connect("ModDo", false, "gatewaypub", "gateway-pass");
		if (returnCode != ConnectReturnCode.ACCEPTED) {
			System.out.println("Unable to connect to the broker. Reason: " );
			return;
		}

		// Publish a musical catalog
		client.publish(new PublishMessage(topics, QoS.AT_MOST_ONCE, messages));
		

		//client.publish(new PublishMessage("messages", QoS.AT_MOST_ONCE, "World"));
		
	} catch (Exception ex) {
		System.out.println("An exception prevented the publishing of the full catalog.");
	} finally {
		// We are done. Disconnect.
		if (!client.isClosed()) {
			client.disconnect();
		}
	}
	
	
}

}
