package com.JuhoMikael.ModbusMQTTGateway;


import net.wimpi.modbus.procimg.SimpleDigitalIn;
import net.wimpi.modbus.procimg.SimpleDigitalOut;
import net.wimpi.modbus.procimg.SimpleInputRegister;
import net.wimpi.modbus.procimg.SimpleProcessImage;

public class slaveProcessImage implements Runnable {

		
		SimpleProcessImage spi = new SimpleProcessImage();
		
		SimpleDigitalIn DI1 = new SimpleDigitalIn(false);
		
		SimpleDigitalIn DI2 = new SimpleDigitalIn(false);
		
		SimpleDigitalIn DI3 = new SimpleDigitalIn(false);
		
		
		SimpleInputRegister R1 = new SimpleInputRegister(21);
		SimpleInputRegister R2 = new SimpleInputRegister(21);
		
		SimpleDigitalOut DO1 = new SimpleDigitalOut();
		SimpleDigitalOut DO2 = new SimpleDigitalOut();
		
	public void changeStateDI1(String state){
		
		if(state == "on")
		DI1.set(true);
		
		else
		DI1.set(false);
	}

	public void changeStateDI2(String state){
		
		if(state == "on")
		DI2.set(true);
		
		else
		DI2.set(false);
	}

	public void changeStateDI3(String state){
		
		if(state == "on")
		DI3.set(true);
		
		else
		DI3.set(false);
	}

	public void changeValueRegister(int val){
		R1.setValue(val);
	}

	public void changeValueRegister2(int val){
		R2.setValue(val);
	}

	public boolean getStateDI1(){
		return DI1.isSet();
	}

	public boolean getStateDI2(){
		return DI2.isSet();
	}

	public boolean getStateDI3(){
		return DI3.isSet();
	}

	public int getValRegister(){
		return R1.getValue();
	}

	public int getValRegister2(){
		return R2.getValue();
	}

	public void run() {
		
		new Thread(new Runnable() {
			@Override public void run() {

		                MQTTSub rdi1 = new MQTTSub();
		                MQTTPub rdo = new MQTTPub();
		                
		              
		                

		        		new Thread(new Runnable() {
		        			@Override public void run() {
		                rdi1.mqttRef("di1", "di2", "di3", "re1", "re2");
		                try {
							Thread.sleep(500);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
		        			}}).start();
		  
		                 
		while(true) { 
		                 DI1.set(rdi1.getDI());
		              
		                 DI2.set(rdi1.getDI2());
		                 DI3.set(rdi1.getDI3());
		                 R1.setValue(Integer.parseInt(rdi1.getReg1()));
		                 R2.setValue(Integer.parseInt(rdi1.getReg2()));
		                 
		                 if(DO1.isSet() && DO2.isSet()){
		                	 System.out.println("error twin state");
		                 }
		                 else if (DO1.isSet()){
		                	 rdo.publish("LOCK","1");
		                	 DO1.set(false);
		                 }
		                 else if (DO2.isSet()){
		                	 rdo.publish("LOCK","0");
		                	 DO2.set(false);
		                 }  
		                 
		                 try {
							Thread.sleep(500);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

		        }
		    
			}}).start();
		
		
		spi.addDigitalIn(DI1);
		spi.addDigitalIn(DI2);
		spi.addDigitalIn(DI3);
		spi.addInputRegister(R1);
		spi.addInputRegister(R2);
		spi.addDigitalOut(DO1);
		spi.addDigitalOut(DO2);
	}

}
